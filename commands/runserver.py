from service_api.app import app


def runserver(host, port, auto_reload=False, debug=False, workers=1):
    """Setups params and run server.

    Args:
        host (str): Host where server will be running.
        port (str): Port where server will be running.
        auto_reload (bool): Activate or deactivate the Automatic Reload.
        debug (bool): Activate or deactivate debug mode.
        workers (int): Number of workers.

    """
    app.run(host=host, port=port, auto_reload=auto_reload, debug=debug, workers=workers)

