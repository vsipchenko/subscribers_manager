FROM python:3.7

WORKDIR /app
COPY . /app

# Install packages
RUN pip install --no-cache-dir -r requirements.txt

# Expose a port for runserver
EXPOSE 8000

# Runserver
ENTRYPOINT  python manage.py runserver -h 0.0.0.0 -p 8000
