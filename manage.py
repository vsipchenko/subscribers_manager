import argparse
import sys
import logging

import commands as cmd

logging.basicConfig(filename='app.log', filemode='w', level=logging.DEBUG, format='%(levelname)s - %(message)s')


def parse_args(args):
    parser = argparse.ArgumentParser(description="Sanic rest api skeleton", add_help=False)
    parser.add_argument("--help", action="help", help="show this help message and exit")

    subparsers = parser.add_subparsers(dest="command")

    sparser = subparsers.add_parser(cmd.runserver.__name__, add_help=False, help="Runserver on specific port")
    sparser.add_argument("-h", "--host", dest="host", default="0.0.0.0", type=str, help="Host address")
    sparser.add_argument("-p", "--port", dest="port", default=8000, type=int, help="Host post")
    sparser.add_argument("-a", "--autoreload", dest="auto_reload", default=False, type=bool)
    sparser.add_argument("-d", "--debug", dest="debug", default=False, type=bool)
    sparser.add_argument("-w", "--workers", dest="workers", default=1, type=int)

    return parser.parse_args(args=args)


def main(args=None):

    parsed_args = parse_args(args or sys.argv[1:])

    if parsed_args.command == cmd.runserver.__name__:
        cmd.runserver(parsed_args.host, parsed_args.port)

    raise Exception("Invalid command")


if __name__ == "__main__":
    try:
        logging.info('Server started')
        main()
    except Exception as e:
        logging.error(f'Error! {e}')
        exit(1)
    finally:
        logging.info("Service stopped.")
