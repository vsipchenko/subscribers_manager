import os

from service_api.constants import SERVICE_NAME


class DevConfig:
    SERVICE_NAME = SERVICE_NAME
    MONGO_DB_NAME = os.environ.get("MONGO_DB_NAME", "subscribers_manager")
    MONGO_DB_HOST = os.environ.get("MONGO_DB_HOST", "localhost")
    MONGO_DB_PORT = os.environ.get("MONGO_DB_PORT", "27017")
    NOTIFICATION_MANAGER_ADDRESS = f'http://{os.environ.get("NOTIFICATION_MANAGER_HOST")}:{os.environ.get("NOTIFICATION_MANAGER_PORT")}'


ENV_CONFIG = {"dev": DevConfig}


def runtime_config(config=None):
    if config is None:
        env = os.environ.get("APP_ENV", "dev")
        config = ENV_CONFIG[env]
    return config
