from vsipc_tools.base_rest_client import BaseRestApiClient

from service_api.app import app


class NotificationManagerRESTClient(BaseRestApiClient):
    host = app.config['NOTIFICATION_MANAGER_ADDRESS']

    async def generate_report(self, data=''):
        reports_generate_uri = '/notification-manager/reports/generate'
        return await self.post(self.host + reports_generate_uri, body=data)
