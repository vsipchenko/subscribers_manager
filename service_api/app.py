import motor.motor_asyncio
from sanic import Sanic

from service_api.config import runtime_config
from service_api.constants import SERVICE_NAME


app = Sanic(name=SERVICE_NAME)
app.config.from_object(runtime_config())


@app.listener("before_server_start")
async def before_server_start(app, loop):
    app.mongo_client = motor.motor_asyncio.AsyncIOMotorClient(
        f"mongodb://{app.config.MONGO_DB_HOST}:{app.config.MONGO_DB_PORT}/", io_loop=loop)


@app.listener("after_server_start")
async def after_server_start(app, loop):
    from service_api import api
    api.load_api(app)
