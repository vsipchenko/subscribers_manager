from sanic import Blueprint
from sanic.app import Sanic


def load_api(app: Sanic):
    from service_api.resources.subscribers import SubscribersResources, SubscribersDetailResources

    api_prefix = f"/{app.config.get('SERVICE_NAME')}/subscribers/"
    api = Blueprint("main", url_prefix=api_prefix, strict_slashes=False)

    # get the endpoints here
    api.add_route(SubscribersResources.as_view(), "", strict_slashes=False)
    api.add_route(SubscribersDetailResources.as_view(), "<subscriber_id:string>", strict_slashes=False)
    app.blueprint(api)
