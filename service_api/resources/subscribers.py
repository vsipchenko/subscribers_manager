import os

import aiohttp
from sanic.response import json
from sanic.views import HTTPMethodView

from service_api.app import app
from service_api.domain.subscribers.controller import SubscribersController
from service_api.rest_clients import notification_manager_rest_client


class BaseSubscribersResources(HTTPMethodView):
    controller = SubscribersController()


class SubscribersResources(BaseSubscribersResources):
    async def get(self, request):
        return json(
            {
                "data": os.environ.get("SOME_VARIABLE", "variable not found"),
                "subscribers": await self.controller.get_subscribers()
            }
        )

    async def post(self, request):
        data = await self.controller.save_single_subscriber(request.json)
        notification_info = await notification_manager_rest_client.generate_report(request.json)
        return json({'data': data, 'notification_info': notification_info}, status=201)


class SubscribersDetailResources(BaseSubscribersResources):
    async def get(self, request, subscriber_id):
        return json({"data": f"SubscribersDetailResources get {subscriber_id}"})

    async def put(self, request, subscriber_id):
        return json({"data": f"SubscribersDetailResources put {subscriber_id}"})

    async def delete(self, request, subscriber_id):
        return json({"data": f"SubscribersDetailResources delete {subscriber_id}"})
