from bson import ObjectId

from service_api.utils import MongoIdConverter
from service_api.domain.base.repository import BaseRepository


class SubscribersRepository(BaseRepository):
    collection_name = "subscribers"

    async def get_one_by_id(self, _id):
        return await self.collection.find_one({"_id": ObjectId(_id)})

    async def get_all(self, length=100):
        subscribers = await self.collection.find({}).to_list(length=length)
        return MongoIdConverter.id_to_string(subscribers)

    async def insert_one(self, data):
        item = await self.collection.insert_one(data)
        return MongoIdConverter.id_to_string(item.inserted_id)

    async def insert_multiple(self, data):
        # TODO: not ready yet
        item = await self.collection.insert_one(data)
        return MongoIdConverter.id_to_string(item)
