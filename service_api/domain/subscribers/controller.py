from service_api.domain.subscribers.repository import SubscribersRepository


class SubscribersController:
    repository = SubscribersRepository()

    async def get_subscribers(self):
        return await self.repository.get_all()

    async def save_single_subscriber(self, data):
        return await self.repository.insert_one(data)
