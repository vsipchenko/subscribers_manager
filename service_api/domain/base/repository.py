from service_api.app import app


class BaseRepository:
    db = app.mongo_client[app.config.MONGO_DB_NAME]
    collection_name = None

    def __init__(self):
        self.collection = self.db[self.collection_name]
